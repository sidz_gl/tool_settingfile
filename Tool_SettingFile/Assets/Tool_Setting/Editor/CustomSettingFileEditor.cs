﻿/*AUTHOR:SIDZ_CORP
 Source:https://sidzrandomstuff.wordpress.com/
 NAMESPACE: ZDIS.TOOLS.
 DATE:21-12-2018
 NOTE: Use this for whatever purpose you would want to use it for.But it would be nice if you can Visit  MY Site.
*/
using UnityEditor;
using UnityEngine;

namespace ZDIS.Tools
{
    //INFO:-
    /*This class is a simple editor script to better manage and display the created SCriptble object*/

    [CustomEditor(typeof(CustomSettingFile))]
    public class CustomSettingFileEditor : Editor
    {
        private CustomSettingFile m_refCustomSettingFile;
        public void OnEnable()
        {
            m_refCustomSettingFile = target as CustomSettingFile;

            //Check if streaming Assets folder is there are not.
            if(!System.IO.Directory.Exists(Application.streamingAssetsPath))
            {
                Debug.LogWarning("CustomSettingFileEditor:OnEnable:Creating Straming Assets folder...");
                System.IO.Directory.CreateDirectory(Application.streamingAssetsPath);
            }
            else
            {
                Debug.LogWarning("CustomSettingFileEditor:OnEnable:Straming Assets folder already created...");
            }
        }
        public void OnDisable()
        {

        }
        public override void OnInspectorGUI()
        {
            if (m_refCustomSettingFile)
            {
                m_refCustomSettingFile.CheckStatus();

                switch(m_refCustomSettingFile._CurrentSettingStatus)
                {
                    case CustomSettingFile.eSettingFileStatus.NoSettingFile:
                        EditorGUILayout.HelpBox("No Settings file Created:", MessageType.Error);
                        if (GUILayout.Button("Resolve it!"))
                        {
                            m_refCustomSettingFile.WriteAllData();
                        }

                        break;
                    case CustomSettingFile.eSettingFileStatus.MisMatchSettingFile:
                        EditorGUILayout.HelpBox("The saved Settings Files has different Values:\n"+ m_refCustomSettingFile.StrFileIntergrityStatusMessageInfo, MessageType.Error);
                        if (GUILayout.Button("Resolve it!"))
                        {
                            m_refCustomSettingFile.WriteAllData();
                        }
                        break;

                    case CustomSettingFile.eSettingFileStatus.MissingGeneratedFile:
                        EditorGUILayout.HelpBox("The GEnerated file is Missing:\n" + m_refCustomSettingFile.StrFileIntergrityStatusMessageInfo, MessageType.Error);
                        if (GUILayout.Button("Resolve it!"))
                        {
                            m_refCustomSettingFile.WriteAllData();
                        }
                        break;

                    case CustomSettingFile.eSettingFileStatus.AllFilesUpdated:
                        EditorGUILayout.HelpBox("All Settings are Up to Date!", MessageType.Info);
                        break;
                }
               

            }
           
            base.OnInspectorGUI();
         
        }
    }
}