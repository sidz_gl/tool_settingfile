﻿/*AUTHOR:SIDZ_CORP
 Source:https://sidzrandomstuff.wordpress.com/
 NAMESPACE: ZDIS.TOOLS.
 DATE:21-12-2018
 NOTE: Use this for whatever purpose you would want to use it for.But it would be nice if you can Visit  MY Site.
*/
using System.IO;
using UnityEditor;
using UnityEngine;
namespace ZDIS.Tools
{
    //INFO:-
    /*This class is a simple editor script to create your SCriptable Object.*/
    public class SettingCreatorUtil
    {
        
        [MenuItem("Assets/CreateCustomSetting/SettingFileForYourGame")]
        public static void CreateAsset()
        {
            CreateAsset<CustomSettingFile>();
        }
        public static void CreateAsset<T>() where T : ScriptableObject
        {
            T asset = ScriptableObject.CreateInstance<T>();

            string temp_strFileFullPath = Path.Combine(Application.dataPath, SettingHelper.c_strCustomSettingFileFolder);
            if (!Directory.Exists(temp_strFileFullPath))
            {
                Directory.CreateDirectory(temp_strFileFullPath);
            }

       

            string path = AssetDatabase.GetAssetPath(Selection.activeObject);
         
            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath("Assets/"+SettingHelper.c_strCustomSettingFileFolder + "/" + "MyCustomSettingFile" + ".asset");
            AssetDatabase.CreateAsset(asset, assetPathAndName);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;

        }

    }
}