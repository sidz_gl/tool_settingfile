/*AUTHOR:SIDZ_CORP
 Source:https://sidzrandomstuff.wordpress.com/
 NAMESPACE: ZDIS.TOOLS.
 DATE:21-12-2018
 NOTE: Use this for whatever purpose you would want to use it for.But it would be nice if you can Visit  MY Site.
*/

// ----- AUTO GENERATED CODE ----- //
// ----- Created through Settings helper ----- //
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;


public class Settings_Sample
{
    private static bool bIsFileLoaded = false;
    private const string c_strFileName = "Settings_Sample.txt"; //Pump in values here
    private static readonly string[] c_strProprty = { "ArrowSpeed", "Index" ,};//available values...

    #region PREHECKS_LOGIC
    /* Steps
     ** <At start set file loading to true,as it should be done inrespective of success or failure...>
     1.Check if in editor or not
     2.Read the File
     3.Check if Property is there
     4.If so,load that value to the class value
     */
    private static void PreChecks()
    {

        Settings_Sample s = new Settings_Sample();

        Type typeInQuestion = typeof(Settings_Sample);
        // FieldInfo field = typeInQuestion.GetField("ArrowSpeed", BindingFlags.Static | BindingFlags.NonPublic);
       // field.SetValue(s, 100.0f);
        FieldInfo field = null;
        var temp_refFieldsArray = typeInQuestion.GetFields(BindingFlags.Static | BindingFlags.NonPublic);

        for (int i = 0; i < temp_refFieldsArray.Length; i++)
        {
            Debug.LogError("" + temp_refFieldsArray[i].Name + ",Value:" + temp_refFieldsArray[i].GetValue(s));
        }
       

        //referenceDebug.LogError( "Can we see this"+ s.GetType().GetProperty("ArrowSpeed", typeof(float)).GetValue(s, null));
        Debug.LogError("Can we see this" + ArrowSpeed);

        /*Dont' run this in EDITOR,read directly from scriptable object...*/
        if (!bIsFileLoaded && !Application.isEditor)
        {
            List<ZDIS.Tools.CustomSettingFile.SettingData> Out_lstSettingData = new List<ZDIS.Tools.CustomSettingFile.SettingData>();
            bIsFileLoaded = true;
            string temp_strFilePath = ZDIS.Tools.SettingHelper.GetMyPath(c_strFileName);
            var temp_refReadLst = ZDIS.Tools.SettingHelper.ReadFromFile(temp_strFilePath);
            /*Loop here*/
            for (int i = 0; i < c_strProprty.Length; i++)
            {
                if (ZDIS.Tools.SettingHelper.SearchForPropertyInlLst(temp_refReadLst, c_strProprty[i], out Out_lstSettingData))
                {

                    /*Update the class value..*/
                    string temp_strValue = Out_lstSettingData[0].ValueInSystemType();
                    System.Object temp_refObjValue = new System.Object();
                    if (Out_lstSettingData[0].GetActualValueType(out temp_refObjValue))
                    {
                        field = typeInQuestion.GetField(c_strProprty[i], BindingFlags.Static | BindingFlags.NonPublic);
                        if (field != null)
                        {
                            field.SetValue(s, temp_refObjValue);
                            Debug.LogError("ReadFromFile Passed!," + temp_strFilePath + ",property name:" + c_strProprty[i] + "will use read Values...newvalue:" + field.GetValue(s));/*//variable*/
                        }
                        else
                        {
                            Debug.LogError("ReadFromFile failed,Field not found!," + temp_strFilePath + ",property name:" + c_strProprty[i] + "will use read Values...newvalue:" + field.GetValue(s));/*//variable*/
                        }
                    }
                }
                else
                {

                    Debug.LogError("ReadFromFile but failed," + temp_strFilePath + ",property name:" + c_strProprty[i] + "will use class values..");/*//variable*/
                }

            }
        }
    }

    #endregion PREHECKS_LOGIC

    #region SETTING_VARIABLE

    private static float ArrowSpeed = 2.5f;
    public static float _ArrowSpeed { get { PreChecks(); return ArrowSpeed; } }

    private static int Index = 100;
    public static int _Index { get { return Index; } }

    #endregion SETTING_VARIABLE
}
