﻿/*AUTHOR:SIDZ_CORP
 Source:https://sidzrandomstuff.wordpress.com/
 NAMESPACE: ZDIS.TOOLS.
 DATE:21-12-2018
 NOTE: Use this for whatever purpose you would want to use it for.But it would be nice if you can Visit  MY Site.
*/

//INFO:-
/*This  class  holds the basic structure of the all the settings parameters.It is incharge of creating the 
 scsriptable object,writing creating,modifying class files
 --FOR now we are using JSON formats to write.May be later we can change it to something generic*/

using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;

namespace ZDIS.Tools
{

    //  [CreateAssetMenu(filename ="s", menuName = "")]
    //  [CreateAssetMenu(fileName = "CustomSettingFile", menuName = "CreateCustomSetting/SettingFileForYourGame")]
    //We have own editor to create it!
    public class CustomSettingFile : ScriptableObject
    {
        public enum eSettingFileStatus
        {
            NoSettingFile,
            AllFilesUpdated,
            MisMatchSettingFile,

            MissingGeneratedFile,//Class file which is created
        }


        public enum eValueType
        {
            Int,
            Bool,
            Float,
            String
        }
        [System.Serializable]
        public class SettingData
        {
            [SerializeField] private string m_strName = "Enter_A_Name";
            [SerializeField] private string m_strValue = "Enter_A_Value";
            [SerializeField] private eValueType m_eValueType = eValueType.String;//FUTURE CODE GENERATION

            public SettingData()
            {
                m_strName = "Enter_A_Name";
                m_strValue = "Enter_A_Value";
                m_eValueType = eValueType.String;
            }

            public string Name
            {
                get
                {
                    return m_strName;
                }

                set
                {
                    m_strName = value;
                }
            }

            public string Value
            {
                get
                {
                    return m_strValue;
                }

                set
                {
                    m_strValue = value;
                }
            }

            public eValueType ValueType
            {
                get
                {
                    return m_eValueType;
                }

                set
                {
                    m_eValueType = value;
                }
            }

            /// <summary>
            /// Get the acutal object value
            /// </summary>
            /// <param name="a_outObjValue"></param>
            /// <returns></returns>
            public bool GetActualValueType(out object a_outObjValue)
            {

                bool bParseResult = false;
                a_outObjValue = null;
                switch (ValueType)
                {
                    case eValueType.Bool:
                        bool temp_bResult = false;
                        bParseResult = bool.TryParse(Value, out temp_bResult);

                        if (bParseResult)
                        {
                            if (temp_bResult)
                            {
                                a_outObjValue = true;
                            }

                        }
                        a_outObjValue = false;
                        return true;

                    case eValueType.Float:
                        float temp_fResult = 0.0f;
                        bParseResult = float.TryParse(Value, out temp_fResult);
                        if (bParseResult)
                        {
                            a_outObjValue = temp_fResult;
                        }
                        else
                        {
                            a_outObjValue = 0.0f;
                        }
                        return true;

                    case eValueType.Int:
                        int temp_iResult = 0;
                        bParseResult = int.TryParse(Value, out temp_iResult);
                        if (bParseResult)
                        {
                            a_outObjValue = temp_iResult;
                        }
                        else
                        {
                            a_outObjValue = 0;
                        }
                        return true;

                    case eValueType.String:
                        a_outObjValue = Value;
                        return true;


                }
                return false;

            }

            /// <summary>
            /// This func gives System.Type in string...:)
            /// </summary>
            /// <returns></returns>
            public string ValueTypeInSystem()
            {
                switch (ValueType)
                {
                    case eValueType.Bool:
                        return "bool";
                    case eValueType.Float:
                        return "float";
                    case eValueType.Int:
                        return "int";
                    case eValueType.String:
                        return "String";

                    default:
                        return "String";
                }
            }

            /// <summary>
            /// This func gives value in String for class wrting purposes
            /// </summary>
            /// <returns></returns>
            public string ValueInSystemType()
            {
                bool bParseResult = false;
                switch (ValueType)
                {
                    case eValueType.Bool:

                        bool temp_bResult = false;
                        bParseResult = bool.TryParse(Value, out temp_bResult);
                        if (bParseResult)
                        {
                            if (temp_bResult)
                            {
                                return "true";
                            }

                        }
                        else
                        {
                            return "false";
                        }
                        return "false";

                    case eValueType.Float:

                        float temp_fResult = 0.0f;
                        bParseResult = float.TryParse(Value, out temp_fResult);
                        if (bParseResult)
                        {
                            return temp_fResult + "f";
                        }
                        else
                        {
                            return "0.0f";
                        }


                    case eValueType.Int:
                        int temp_iResult = 0;
                        bParseResult = int.TryParse(Value, out temp_iResult);
                        if (bParseResult)
                        {
                            return "" + temp_iResult;
                        }
                        else
                        {
                            return "0";
                        }

                    case eValueType.String:
                        return "\"" + Value + "\"";

                    default:
                        return "ERROR";
                }

            }
            public bool IsSameAs(SettingData a_OtherSettingData)
            {
                return string.Equals(a_OtherSettingData.Name, Name) &&
                          string.Equals(a_OtherSettingData.Value, Value) &&
                          string.Equals(a_OtherSettingData.ValueType, ValueType);
            }

        }


        [Header("CustomSettingFile:READ ONLY STUFF")]
        [SerializeField] private eSettingFileStatus m_eCurrentSettingStatus = eSettingFileStatus.NoSettingFile;//READ ONLY
        [Header("CustomSettingFile:Settings to Have")]
        [SerializeField] private List<SettingData> m_lstSettingData;

        [SerializeField]
        public class WrapperListSettingData
        {
            public List<SettingData> m_lstSettingData = new List<SettingData>();
        }




        private Dictionary<string, string> m_PropertyMap = new Dictionary<string, string>();
        private string m_strName = string.Empty;

        public eSettingFileStatus _CurrentSettingStatus
        {
            get
            {
                return m_eCurrentSettingStatus;
            }


        }
        /// <summary>
        /// Editor Message and Current Error Status with more info...
        /// </summary>
        private string m_strFileIntergrityStatusMessageInfo = "";
        public string StrFileIntergrityStatusMessageInfo
        {
            get
            {
                return m_strFileIntergrityStatusMessageInfo;
            }

        }


        #region GETTER_SETTINGS
        public string GetProperty(string a_strProprtyName)
        {

            if (m_lstSettingData == null)
            {
                Debug.LogError("GetProperty()=>List setting data is NULL");
                return string.Empty;
            }
            if (m_lstSettingData.Count == 0)
            {
                Debug.LogError("GetProperty()=>List if setting data is ZERO");
                return string.Empty;
            }
            var lstResult = m_lstSettingData.Where(s => s.Name == a_strProprtyName).ToList<SettingData>();
            if (lstResult == null)
            {
                Debug.LogError("GetProperty()=>Property not found" + a_strProprtyName);
                return string.Empty;
            }
            if (lstResult.Count == 0)
            {
                Debug.LogError("GetProperty()=>Property not found" + a_strProprtyName);
                return string.Empty;
            }
            if (lstResult.Count > 1)
            {
                Debug.LogError("GetProperty()=>More than one found...will used the first one..." + a_strProprtyName);
            }

            return lstResult[0].Value;

        }

        public int GetPropertyAsInt(string a_strProprtyName)
        {
            string temp_strValue = GetProperty(a_strProprtyName);
            int temp_iResult = -999999;

            if (!int.TryParse(temp_strValue, out temp_iResult))
            {
                Debug.LogError("GetPropertyAsInt() property is empty:" + temp_strValue + ",converted int" + temp_iResult);
                return -999999;
            }

            return temp_iResult;
        }
        //Do same for others...

        #endregion GETTER_SETTINGS

        #region FILE_OPERATION


        [ContextMenu("Check the status")]
        public void CheckStatus()
        {
            if (!File.Exists(GetMyPath()))
            {
                m_eCurrentSettingStatus = eSettingFileStatus.NoSettingFile;
                //  Debug.LogError("File doesnt exist:" + GetMyPath());

                return;
            }
            var temp_fileContent = ReadFromFile();
            if (temp_fileContent == null)
            {
                m_eCurrentSettingStatus = eSettingFileStatus.NoSettingFile;
                Debug.LogError("Coundnt readfile at ...:" + GetMyPath());
                return;
            }

            //Check for generated file here
            /*     string temp_strFileFullPath = Path.Combine(Application.dataPath, c_strCustomSettingFileFolder);

            if(!Directory.Exists(temp_strFileFullPath))
            {
                Directory.CreateDirectory(temp_strFileFullPath);
            }

            temp_strFileFullPath = Path.Combine(temp_strFileFullPath, c_strCustomSettingFileGeneratedFolder);*/
            string temp_strGenFilePath = Path.Combine(Application.dataPath, SettingHelper.c_strCustomSettingFileFolder);
            temp_strGenFilePath = Path.Combine(temp_strGenFilePath, SettingHelper.c_strCustomSettingFileGeneratedFolder);
            temp_strGenFilePath = Path.Combine(temp_strGenFilePath, "Settings_" + this.name + ".cs");

            if (!File.Exists(temp_strGenFilePath))
            {
                m_eCurrentSettingStatus = eSettingFileStatus.MissingGeneratedFile;
               // Debug.LogError("Missing Generated File,need to create it:" + temp_strGenFilePath);
                return;
            }


            if (CheckIfFileIntegrity(temp_fileContent))
            {
                m_eCurrentSettingStatus = eSettingFileStatus.AllFilesUpdated;
            }
            else
            {
                m_eCurrentSettingStatus = eSettingFileStatus.MisMatchSettingFile;
            }

        }


        [ContextMenu("LoadFromFile")]
        public void LoadFromFile()
        {
            List<SettingData> temp_refSettingData = ReadFromFile();
            if (temp_refSettingData == null)
            {
                Debug.LogError("LoadFromFile()=>The file cannot me read!");
                return;

            }
            if (temp_refSettingData.Count == 0)
            {
                Debug.LogError("LoadFromFile()=>None returned!");
                return;
            }

            if (!CheckIfFileIntegrity(temp_refSettingData))
            {
                Debug.LogError("Settings file has different property compared to one present here.");
                return;
            }
            m_lstSettingData = temp_refSettingData;
        }




        #region TESTING_PLAYGROUND

        [ContextMenu("Write To File")]/*Use this for writing and create class files...*/
        public void WriteAllData()
        {
            if (m_lstSettingData == null)
            {
                Debug.LogError("The List of Settings DAta is null");
                return;
            }
            if (m_lstSettingData.Count == 0)
            {
                Debug.LogError("The List of Settings DAta is Zero");
                return;
            }
            bool m_bWriteResult = false;

            m_bWriteResult = WriteToFile(m_lstSettingData);
            if (!m_bWriteResult)
            {
                Debug.LogError("Not AbleTowrite to File,Setting Data list name");
                return;
            }

            SettingHelper.CreateSettingClass(SettingHelper.c_strGenFilePrefix + this.name, m_lstSettingData);
            Debug.Log("All Settings data has been updated to:" + GetMyPath());
        }
        #endregion TESTING_PLAYGROUND






        public string GetMyPath()
        {
            string temp_strFileName = "GameSettings.txt";//TODO later Testing******************

            temp_strFileName = this.name + ".txt";
            string path = Path.Combine(Application.streamingAssetsPath, temp_strFileName);
            return path;
        }


        /// <summary>
        /// This function checks if the file read has the settings present in delcared SO
        /// </summary>
        /// <returns></returns>
        public bool CheckIfFileIntegrity(List<SettingData> a_lstSettingData)
        {
            if (a_lstSettingData == null)
            {
                m_strFileIntergrityStatusMessageInfo = "CheckIfFileIntegrity:  Given Setting itself is NULL";
                Debug.LogError(m_strFileIntergrityStatusMessageInfo);
                return false;
            }
            if (m_lstSettingData == null)
            {
                m_strFileIntergrityStatusMessageInfo = "CheckIfFileIntegrity:  Our Setting itself is NULL";
                Debug.LogError(m_strFileIntergrityStatusMessageInfo);
                return false;
            }
            if (m_lstSettingData.Count != a_lstSettingData.Count)
            {
                m_strFileIntergrityStatusMessageInfo = "CheckIfFileIntegrity:Setting Values Count MisMatch,\n" + "FromFile Settings Count:" + a_lstSettingData.Count + "\n Current SettingsCount:" + m_lstSettingData.Count;
                Debug.LogError(m_strFileIntergrityStatusMessageInfo);

                return false;
            }
            for (int i = 0; i < a_lstSettingData.Count; i++)
            {
                if (!a_lstSettingData[i].IsSameAs(m_lstSettingData[i]))
                {
                    m_strFileIntergrityStatusMessageInfo = " CheckIfFileIntegrity: Mismatch of values,\n Current:" + m_lstSettingData[i].Name + ":Value:" + m_lstSettingData[i].Value + "\n FromFile:" + a_lstSettingData[i].Name + ":Value:" + a_lstSettingData[i].Value;
                    Debug.LogError(m_strFileIntergrityStatusMessageInfo);
                    return false;
                }
            }
            return true;
        }


        public bool CheckSettingsFileExist(string a_strPath)
        {
            string temp_strFileName = "Settings.txt";//TODO later Testing

            string path = Path.Combine(Application.streamingAssetsPath, temp_strFileName);

            Debug.Log("CheckSettingsFileExist()+>Location" + path);
            if (!File.Exists(path))
            {
                Debug.Log("");
                return true;
            }
            return false;
        }




        public List<SettingData> ReadFromFile()
        {
            List<SettingData> temp_lstSettingData = new List<SettingData>();
            if (!File.Exists(GetMyPath()))
            {
                Debug.LogError("ReadFromFile it doesnt Exists at path" + GetMyPath());
                return null;
            }

            try
            {
                string temp_strReadContent = File.ReadAllText(GetMyPath());
                var temp_refWrapperlist = JsonUtility.FromJson<WrapperListSettingData>(temp_strReadContent);
                temp_lstSettingData = temp_refWrapperlist.m_lstSettingData;

                return temp_lstSettingData;


            }
            catch (System.Exception e)
            {
                Debug.LogError("Expection Caught:" + e.Message);
                return null;
            }


        }

        public bool WriteToFile(List<SettingData> a_lstSettingData)
        {


            WrapperListSettingData temp_wrapData = new WrapperListSettingData();

            temp_wrapData.m_lstSettingData = a_lstSettingData;
            string strJson = JsonUtility.ToJson(temp_wrapData,true);

            string path = Path.Combine(Application.streamingAssetsPath, GetMyPath());
            StreamWriter sw = null;
            Debug.Log(path);
            try
            {
                sw = new StreamWriter(path, true);
                //  UnityEditor.AssetDatabase.Refresh();

                if (!File.Exists(path))
                {
                    Debug.LogError("StreamWriter used...Must have already created file " + path);
                    return false;
                }
                sw.Close();
                File.WriteAllText(path, strJson);

                Debug.LogError("Before Write..." + strJson);

                string temp_strReadContent = File.ReadAllText(path);
                Debug.LogError("Test:Reading the written File File" + temp_strReadContent + ",Name:" + JsonUtility.FromJson<WrapperListSettingData>(temp_strReadContent).m_lstSettingData[0].Name);
                ;
                return true;
            }
            catch (System.Exception e)
            {
                if (sw != null)
                {
                    sw.Close();
                }
                Debug.LogError("Expection Caught:" + e.Message);
                return false;
            }

        }

        #endregion FILE_OPERATION
    }
}