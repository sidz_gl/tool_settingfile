﻿/*AUTHOR:SIDZ_CORP
 Source:https://sidzrandomstuff.wordpress.com/
 NAMESPACE: ZDIS.TOOLS.
 DATE:21-12-2018
 NOTE: Use this for whatever purpose you would want to use it for.But it would be nice if you can Visit  MY Site.
*/



using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using UnityEngine;
//INFO:-
/*This is a helper class which holds all the constants and function used of gernerating custom classes....*/

namespace ZDIS.Tools
{
    public class SettingHelper
    {

        //Constants...
        public static readonly string c_strDoubleQuote = "\"";
        public static readonly string c_strCustomSettingFileFolder = "CustomSettingsFiles";
        public static readonly string c_strCustomSettingFileGeneratedFolder = "_GeneratedFile";
        public static readonly string c_strGenFilePrefix = "Settings_";


        private static void CreatePreCheckFunc(StringBuilder a_refbuilder, string a_strFileName)
        {
            a_refbuilder.AppendLine("private static void PreChecks()");
            a_refbuilder.AppendLine("{");

            a_refbuilder.AppendLine(a_strFileName + " s = new " + a_strFileName + "();");
            a_refbuilder.AppendLine("Type typeInQuestion = typeof(" + a_strFileName + ");");
            a_refbuilder.AppendLine("FieldInfo field = null;");
            a_refbuilder.AppendLine(" var temp_refFieldsArray = typeInQuestion.GetFields(BindingFlags.Static | BindingFlags.NonPublic);");

       
        
            string sb = "  if (!bIsFileLoaded  && !Application.isEditor)" +
"        {" +
"            List<ZDIS.Tools.CustomSettingFile.SettingData> Out_lstSettingData = new List<ZDIS.Tools.CustomSettingFile.SettingData>();" +
"            bIsFileLoaded = true;" +
"            string temp_strFilePath = ZDIS.Tools.SettingHelper.GetMyPath(c_strFileName);" +
"            var temp_refReadLst = ZDIS.Tools.SettingHelper.ReadFromFile(temp_strFilePath);" +
"            /*Loop here*/" +
"            for (int i = 0; i < c_strProprty.Length; i++)" +
"            {" +
"                if (ZDIS.Tools.SettingHelper.SearchForPropertyInlLst(temp_refReadLst, c_strProprty[i], out Out_lstSettingData))" +
"                {" +
"                    /*Update the class value..*/" +
"                    string temp_strValue = Out_lstSettingData[0].ValueInSystemType();" +
"                    System.Object temp_refObjValue = new System.Object();" +
"                    if (Out_lstSettingData[0].GetActualValueType(out temp_refObjValue))" +
"                    {" +
"                        field = typeInQuestion.GetField(c_strProprty[i], BindingFlags.Static | BindingFlags.NonPublic);" +
"                        if (field != null)" +
"                        {" +
"                            field.SetValue(s, temp_refObjValue);" +
"                            Debug.LogError(\"ReadFromFile Passed!,\" + temp_strFilePath + \",property name:\" + c_strProprty[i] + \"will use read Values...newvalue:\" + field.GetValue(s));/*//variable*/" +
"                        }" +
"                        else" +
"                        {" +
"                            Debug.LogError(\"ReadFromFile failed,Field not found!,\" + temp_strFilePath + \",property name:\" + c_strProprty[i] + \"will use read Values...newvalue:\" + field.GetValue(s));/*//variable*/" +
"                        }" +
"                    }" +
"                }" +
"                else" +
"                {" +
"                    Debug.LogError(\"ReadFromFile but failed,\" + temp_strFilePath + \",property name:\" + c_strProprty[i] + \"will use class values..\");/*//variable*/" +
"                }" +
"            }" +
"        }";
            a_refbuilder.AppendLine(sb);
            a_refbuilder.AppendLine("}");

        }
        public static void CreateSettingClass(string a_strFileName, List<CustomSettingFile.SettingData> a_lstSettingData)
        {
            if (a_lstSettingData == null)
            {
                Debug.LogError("CreateSettingClass=>Given Setting Data list is Null");
                return;
            }
           // string temp_strFileFullPath = Path.Combine(Application.dataPath ,a_strFileName + ".cs");
            string temp_strFileFullPath = Path.Combine(Application.dataPath, c_strCustomSettingFileFolder);

            if(!Directory.Exists(temp_strFileFullPath))
            {
                Directory.CreateDirectory(temp_strFileFullPath);
            }

            temp_strFileFullPath = Path.Combine(temp_strFileFullPath, c_strCustomSettingFileGeneratedFolder);
            if (!Directory.Exists(temp_strFileFullPath))
            {
                Directory.CreateDirectory(temp_strFileFullPath);
            }

            temp_strFileFullPath = Path.Combine(temp_strFileFullPath, a_strFileName + ".cs");
            FileStream stream = null;
            StreamWriter writer = null;
            Debug.Log("CreateSettingClass at=>" + temp_strFileFullPath);
            try
            {
                stream = File.Open(temp_strFileFullPath, FileMode.OpenOrCreate, FileAccess.Write);
                writer = new StreamWriter(stream);

                StringBuilder builder = new StringBuilder();


                builder.AppendLine("//--AUTHOR:ZDIS_CORP--//");
                builder.AppendLine("//--NAMESPACE: ZDIS.TOOLS.--//");
                builder.AppendLine("//--NOTE: Use this for whatever purpose you would want to use it for.But it would be nice if  you can link back to Site or source.");
                builder.AppendLine("//--Source:https://sidzrandomstuff.wordpress.com");


                builder.AppendLine("// ----- AUTO GENERATED CODE ----- //");
                builder.AppendLine("// ----- Created through Settings helper ----- //");

                //Header declaration...
                builder.AppendLine("using System;");
                builder.AppendLine("using System.Collections.Generic;");
                builder.AppendLine("using System.Reflection;");
                builder.AppendLine("using UnityEngine;");
                builder.AppendLine("/*---------------------------------------------------*/");

                builder.AppendLine("public  class " + a_strFileName);
                builder.AppendLine("{");
                //Contents will come here

                //  builder.AppendLine(" public static int Be"+";");//Sample
                string temp_strVariableType = string.Empty;
                string temp_strVaraibleName = string.Empty;
                string temp_strVaraibleValue = string.Empty;


                //Initial lines for common stuff
                builder.AppendLine("private static bool bIsFileLoaded = false; ");
                builder.AppendLine("private const string c_strFileName =" + "\"" + a_strFileName.Replace("Settings_", "") + ".txt" + "\"" + ";");

                string temp_strVaraibleNames = "";
                foreach (CustomSettingFile.SettingData temp_refSettingData in a_lstSettingData)
                {
                    temp_strVaraibleNames += (c_strDoubleQuote + temp_refSettingData.Name + c_strDoubleQuote) + ",";//THIS EXTRA comma id Fine:Tested but ya..lets remove it later...
                }

                builder.AppendLine("private static readonly string[] c_strProprty = { " + temp_strVaraibleNames + " };");

                CreatePreCheckFunc(builder, a_strFileName);


                builder.AppendLine("");
                builder.AppendLine("#region SETTING_VARIABLE");
                builder.AppendLine("");
                //  builder.AppendLine("#region VaraibleDeclaration");
                foreach (CustomSettingFile.SettingData temp_refSettingData in a_lstSettingData)
                {
                    temp_strVariableType = temp_refSettingData.ValueTypeInSystem();
                    temp_strVaraibleName = temp_refSettingData.Name;
                    temp_strVaraibleValue = temp_refSettingData.ValueInSystemType();

                    //Private variable
                    builder.AppendLine(" private static " + temp_strVariableType + " " + temp_strVaraibleName + "  =  " + temp_strVaraibleValue + ";");

                    //public property
                    builder.AppendLine("public static " + temp_strVariableType + " _" + temp_strVaraibleName + "{ get { PreChecks(); return " + temp_strVaraibleName + ";   }   }");
                    builder.AppendLine("");

                }
                builder.AppendLine("#endregion SETTING_VARIABLE");
                // builder.AppendLine("#endregion VaraibleDeclaration");
                builder.AppendLine("}");
                writer.Write(builder.ToString());

            }
            catch (Exception e)
            {
                Debug.LogError("Cannot Create Setting Class:" + e.Message);
                if (File.Exists(temp_strFileFullPath) == true)
                {
                    File.Delete(temp_strFileFullPath);
                    Debug.LogError("File Created By Mistake,:)..Deleted also...");
                }
            }
            writer.Close();
            stream.Close();
#if UNITY_EDITOR
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

        /// <summary>
        /// Gives the complete path
        /// </summary>
        /// <param name="a_strFileName">Name of file with .txt!</param>
        /// <returns></returns>
        public static string GetMyPath(string a_strFileName)
        {
            string path = Path.Combine(Application.streamingAssetsPath, a_strFileName);
            return path;
        }

        public static List<CustomSettingFile.SettingData> ReadFromFile(string a_strFileName)
        {
            List<CustomSettingFile.SettingData> temp_lstSettingData = new List<CustomSettingFile.SettingData>();
            if (!File.Exists(GetMyPath(a_strFileName)))
            {
                Debug.LogError("ReadFromFile it doesnt Exists at path" + GetMyPath(a_strFileName));
                return null;
            }

            try
            {
                string temp_strReadContent = File.ReadAllText(GetMyPath(a_strFileName));
                var temp_refWrapperlist = JsonUtility.FromJson<CustomSettingFile.WrapperListSettingData>(temp_strReadContent);
                temp_lstSettingData = temp_refWrapperlist.m_lstSettingData;

                return temp_lstSettingData;


            }
            catch (System.Exception e)
            {
                Debug.LogError("Expection Caught:" + e.Message);
                return null;
            }


        }

        public static bool SearchForPropertyInlLst(List<CustomSettingFile.SettingData> a_lstSettingData, string a_strProprtyName, out List<CustomSettingFile.SettingData> Out_lstSettingData)
        {
            Out_lstSettingData = null;
            if (a_lstSettingData == null)
            {
                Debug.LogError("GetProperty()=>List setting data is NULL");
                return false;
            }
            if (a_lstSettingData.Count == 0)
            {
                Debug.LogError("GetProperty()=>List if setting data is ZERO");
                return false;
            }
            var lstResult = a_lstSettingData.Where(s => s.Name == a_strProprtyName).ToList<CustomSettingFile.SettingData>();
            if (lstResult == null)
            {
                Debug.LogError("GetProperty()=>Property not found" + a_strProprtyName);
                return false;
            }
            if (lstResult.Count == 0)
            {
                Debug.LogError("GetProperty()=>Property not found" + a_strProprtyName);
                return false;
            }
            if (lstResult.Count >= 1)
            {
                Debug.LogError("GetProperty()=>More than one found...will used the first one..." + a_strProprtyName);

                Out_lstSettingData = lstResult;
                return true;
            }
            return false;

        }





    }
}