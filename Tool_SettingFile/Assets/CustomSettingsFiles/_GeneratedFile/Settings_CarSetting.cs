//--AUTHOR:ZDIS_CORP--//
//--NAMESPACE: ZDIS.TOOLS.--//
//--NOTE: Use this for whatever purpose you would want to use it for.But it would be nice if  you can link back to Site or source.
//--Source:https://sidzrandomstuff.wordpress.com
// ----- AUTO GENERATED CODE ----- //
// ----- Created through Settings helper ----- //
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
/*---------------------------------------------------*/
public  class Settings_CarSetting
{
private static bool bIsFileLoaded = false; 
private const string c_strFileName ="CarSetting.txt";
private static readonly string[] c_strProprty = { "iID","bTurboSpeed","fMaxSpeed","sstrName", };
private static void PreChecks()
{
Settings_CarSetting s = new Settings_CarSetting();
Type typeInQuestion = typeof(Settings_CarSetting);
FieldInfo field = null;
 var temp_refFieldsArray = typeInQuestion.GetFields(BindingFlags.Static | BindingFlags.NonPublic);
  if (!bIsFileLoaded  && !Application.isEditor)        {            List<ZDIS.Tools.CustomSettingFile.SettingData> Out_lstSettingData = new List<ZDIS.Tools.CustomSettingFile.SettingData>();            bIsFileLoaded = true;            string temp_strFilePath = ZDIS.Tools.SettingHelper.GetMyPath(c_strFileName);            var temp_refReadLst = ZDIS.Tools.SettingHelper.ReadFromFile(temp_strFilePath);            /*Loop here*/            for (int i = 0; i < c_strProprty.Length; i++)            {                if (ZDIS.Tools.SettingHelper.SearchForPropertyInlLst(temp_refReadLst, c_strProprty[i], out Out_lstSettingData))                {                    /*Update the class value..*/                    string temp_strValue = Out_lstSettingData[0].ValueInSystemType();                    System.Object temp_refObjValue = new System.Object();                    if (Out_lstSettingData[0].GetActualValueType(out temp_refObjValue))                    {                        field = typeInQuestion.GetField(c_strProprty[i], BindingFlags.Static | BindingFlags.NonPublic);                        if (field != null)                        {                            field.SetValue(s, temp_refObjValue);                            Debug.LogError("ReadFromFile Passed!," + temp_strFilePath + ",property name:" + c_strProprty[i] + "will use read Values...newvalue:" + field.GetValue(s));/*//variable*/                        }                        else                        {                            Debug.LogError("ReadFromFile failed,Field not found!," + temp_strFilePath + ",property name:" + c_strProprty[i] + "will use read Values...newvalue:" + field.GetValue(s));/*//variable*/                        }                    }                }                else                {                    Debug.LogError("ReadFromFile but failed," + temp_strFilePath + ",property name:" + c_strProprty[i] + "will use class values..");/*//variable*/                }            }        }
}

#region SETTING_VARIABLE

 private static int iID  =  10;
public static int _iID{ get { PreChecks(); return iID;   }   }

 private static bool bTurboSpeed  =  true;
public static bool _bTurboSpeed{ get { PreChecks(); return bTurboSpeed;   }   }

 private static float fMaxSpeed  =  25.2f;
public static float _fMaxSpeed{ get { PreChecks(); return fMaxSpeed;   }   }

 private static String sstrName  =  "SuperSidz";
public static String _sstrName{ get { PreChecks(); return sstrName;   }   }

#endregion SETTING_VARIABLE
}
